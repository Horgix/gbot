class GBotError(Exception):
  pass

class IssueNotFound(GBotError): 
  def __init__(self, issueID):
    self.issueID = issueID
  def __str__(self):
    return ("Issue #" + str(self.issueID) + " not found")

# GBot

Gbot : a redmine bot initially for GConfs
By Alexis 'Horgix' Chotard

.
├── gbotAnswers.py
├── gbotErrors.py
├── gbot.py
├── irc/              https://bitbucket.org/jaraco/irc
├── jaraco/           Utils needed by jaraco/irc
├── key.txt           Store the Redmine REST API key
├── Makefile
├── more_itertools/   
├── README.md         This file
├── redmine/          http://python-redmine.readthedocs.org/#
├── requests/
├── six/
├── testredmine.py
└── TODO

6 directories, 8 files

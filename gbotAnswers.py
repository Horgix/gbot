class GBotAnswer():
    pass

class RespAnswer(GBotAnswer):
    project = None
    resps = []
    def __init__(self, project, resps):
        self.project = project
        self.resps = resps
    def __str__(self):
        return (self.project.name + " : " + ", ".join(self.resps))

class IssueAnswer(GBotAnswer):
    issue = None
    def __init__(self, issue):
        self.issue = issue
    def __str__(self):
        return ("[" + self.issue.tracker.name
                + " #" + str(self.issue.id)
                + " on " + self.issue.project.name + "] "
                + self.issue.subject + " : "
                + "http://redmine.gconfs.fr/issues/" + str(self.issue.id))

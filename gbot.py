from sys import exit
from redmine import Redmine, ResourceNotFoundError
from gbotAnswers import *
import gbotErrors
import irc.bot
import argparse
import itertools
import re

class Tools():
  redmine = None
  def __init__(self, redmine):
    self.redmine = redmine

  def getAuthor(self, event, lowercase = False):
    if lowercase:
      return event.source.nick.lower()
    else:
      return event.source.nick

  def getMessage(self, event, lowercase = False):
    if lowercase:
      return event.arguments[0].lower()
    else:
      return event.arguments[0]

  def parseIssueID(self, msg):
    match = re.findall("#[0-9]+", msg)
    answers = []
    errors = []
    for matched in match:
      issuenb = int(matched[1:])
      try:
        issue = self.redmine.issue.get(issuenb)
        answers.append(IssueAnswer(issue))
      except ResourceNotFoundError:
        errors.append(gbotErrors.IssueNotFound(issuenb))
    return (answers, errors)

  #def searchProjects(self, identifiers):
  #  return [p for p in self.redmine.project.all() if p["identifier"].lower() \
  #      in identifiers]

  def parseResps(self, args): # FIXME Dirty
    answers = []
    for identifier in args:
      project = self.redmine.project.get(identifier)
      for membership in project.memberships:
        for role in membership.roles:
          if role.name == "Respo" and "user" in membership._attributes:
            answers.append(RespAnswer(project, [membership.user.name]))
            # FIXME : Handle when there is more than 1 respo
    return answers

  def parseCmd(self, msg, gbot):
    if len(msg) == 0:
      return None
    command = None
    args = None
    answers = []
    splittedMsg = msg.lower().split()
    # Message formated like "!<cmd> args"
    if splittedMsg[0][:1] == '!':
      command = splittedMsg[0][1:].lower()
      args = splittedMsg[1:]
    # Message formated like "<Bot nickname>[:] <cmd> <args>"
    elif len(splittedMsg) >= 2 and\
        splittedMsg[0].rstrip(':') == gbot.nickname.lower():
      command = splittedMsg[1].lower()
      args = splittedMsg[2:]
    # Treat command and arguments
    if command and args:
      #map(lambda x: x.lower, args) 
      if command in "responsable":
        try:
          answers = self.parseResps(args)
        except ResourceNotFoundError as e:
          return answers # FIXME Handle error instead of that
          #raise TreatmentException("Something not found in parseresp")
    return answers

class Gbot(irc.bot.SingleServerIRCBot):
  # Redmine
  redmineAdress = "http://redmine.gconfs.fr"
  apiKey        = open("key.txt", 'r').readline().rstrip('\n')
  version       = "2.5.2.stable"
  redmine       = None
  # IRC
  serverAdress  = "irc.freenode.org" # Testing server
  serverPort    = 6667
  nickname      = "Wheatley"
  fullname      = "A Redmine bot for GConfs by Horgix"
  maintainer    = "Horgix"
  channel       = "#wheatleytest"
  # Internal use
  debug         = False
  server        = None
  tools         = None
  args          = None

  def __init__(self, args):
    self.redmine = Redmine(self.redmineAdress,
                            key = self.apiKey,
                            version = self.version)
    self.tools = Tools(self.redmine)
    # Read messages fron stdin instead of an IRC connection, for debugging
    if args.debug:
      while True:
        try:
          self.debug = True
          self.treatMsg(input(">>> "))
        except EOFError:
          print("End of debug, stopping")
          exit(0)
    # Take channel and server informations from the CLI arguments if given
    if args.channel:
        self.channel = args.channel
    if args.server:
        self.serverAdress = args.server
    # Launch the bot
    irc.bot.SingleServerIRCBot.__init__(self,
        server_list =  [(self.serverAdress, self.serverPort)],
        nickname = self.nickname, realname = self.fullname)

  def queryMaintainer(self, message):
    self.server.privmsg(self.maintainer, message)

  def bye(self):
    self.queryMaintainer("Bye")
    self.die(msg = "Beep boop the bot is leaving")

  def printAnswer(self, answer, source, isError = False):
    if self.debug:
      print(answer)
    else:
      if isError:
        self.queryMaintainer(answer)
      else:
        self.server.privmsg(source, answer)

  # Treat an IRC message : parse a command starting with '!', parse issues
  # numbers, and answer on the channel it parsed it
  # TODO use source to allow private queries
  def treatMsg(self, msg, source = None):
      cmdAnswers = self.tools.parseCmd(msg, self)
      issuesAnswers, issuesErrors = self.tools.parseIssueID(msg)
      for answer in itertools.chain(cmdAnswers, issuesAnswers):
        self.printAnswer(answer, source)
      for error in issuesErrors:
        self.printAnswer(error, source, isError = True)

  def on_welcome(self, server, event):
    self.server = server
    self.server.join(self.channel)
    self.queryMaintainer("Hey, I just connected to the server")

  def on_privmsg(self, server, event):
    author = self.tools.getAuthor(event)
    message = self.tools.getMessage(event)
    if author == self.maintainer:
      if message == "bye": # Stop the bot in a clean way
        self.bye()
      elif message and message.split()[0] == "say": # Make the bot talk
        self.server.privmsg(self.channel, ' '.join(message.split()[1:]))
    else: # That's my bot, let me stalk what you say to him
      self.queryMaintainer("Someone else (" + author + ") messaged me : "
                            + message)

  def on_pubmsg(self, server, event):
    author = self.tools.getAuthor(event)
    message = self.tools.getMessage(event)
    channel = event.target
    if channel == self.channel:
        self.treatMsg(message, source = channel)

if __name__ == "__main__":
  argparser = argparse.ArgumentParser(description = "A Redmine bot for GConfs\
  by Horgix")
  argparser.add_argument("--debug", action = "store_true",
      help = "Enable debug, parsing commands from stdin instead of IRC")
  argparser.add_argument("--channel",
      help = "Set IRC channel (default: " + Gbot.channel + ")")
  argparser.add_argument("--server",
      help = "Set IRC server (default: " + Gbot.serverAdress + ")")
  args = argparser.parse_args()
  Gbot(args).start()

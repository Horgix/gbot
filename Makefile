all:
	python -B gbot.py
debug:
	python -B gbot.py --debug

run:
	python -B gbot.py --server "irc.rezosup.org" --channel "#gconfs"

clean:
	for cache in `find . -name "__pycache__"`; do rm -rf ${cache}; done

upload:
	scp -r . horgix@horgix.fr:/home/horgix/work/gbot/
